package handler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/flaneurtv/samm-benchmark/config"
	"gitlab.com/flaneurtv/samm-benchmark/model"
	"gitlab.com/flaneurtv/samm-client-go"
)

// CommandHandler handles application commands to melted server
type CommandHandler struct {
	Config *config.Config
}

// HandleBenchmarkTrigger handles benchmark_trigger topic
func (c *CommandHandler) HandleBenchmarkTrigger(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.BenchmarkTrigger{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		logAndRespond("Can't get parse parameters for tick message", err, writer)
		return
	}

	c.runBenchmark(params.Iterations, writer)
}

func (c *CommandHandler) HandleBenchmarkTick(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.Tick{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		logAndRespond("Can't get parse parameters for tick message", err, writer)
		return
	}
	c.runBenchmark(1000, writer)
}

func (c *CommandHandler) HandleBenchmarkPing(message *samm.Message, writer samm.ResponseWriter) {
	params := &model.BenchmarkPing{}
	err := json.Unmarshal(message.Payload, params)
	if err != nil {
		logAndRespond("Can't get parse parameters for tick message", err, writer)
		return
	}
}

func (c *CommandHandler) makeResponse(topic string, payload interface{}, overrideTime ...string) (*samm.Message, error) {
	finalTopic := topic
	if c.Config.NamespacePublisher != "" {
		finalTopic = c.Config.NamespacePublisher + "/" + topic
	}
	response := &samm.Message{
		Topic:       finalTopic,
		ServiceUUID: c.Config.ServiceUUID,
		ServiceName: c.Config.ServiceName,
		ServiceHost: c.Config.ServiceHost,
		CreatedAt:   time.Now().Format(samm.TimeFormat),
	}

	if len(overrideTime) > 0 {
		response.CreatedAt = overrideTime[0]
	}

	if payload != nil {
		var err error
		response.Payload, err = json.Marshal(payload)
		if err != nil {
			return nil, err
		}
	}

	return response, nil
}

func logAndRespond(message string, err error, writer samm.ResponseWriter) {
	writer.WriteError(fmt.Errorf("%s: %s", message, err))
}

func (c *CommandHandler) runBenchmark(iterations int, writer samm.ResponseWriter) {
	u4, err := uuid.NewV4()
	if err != nil {
		writer.WriteError(err)
	}

	start := time.Now()

	for i := 1; i <= iterations; i++ {
		payload := &model.BenchmarkPing{
			BenchmarkUUID: u4.String(),
			PingIndex:     i,
			PingTimestamp: time.Now().Format(samm.TimeFormat),
		}

		response, err := c.makeResponse("default/benchmark/tick", payload)
		if err != nil {
			logAndRespond("Can't marshal tick message params", err, writer)
			return
		}
		writer.WriteObject(response)
	}

	stop := time.Now()
	duration := stop.Sub(start)

	m := `{"topic":"default/benchmark/benchmark_result","payload":{"iterations":%d, "duration": "%s", "ops_per_sec": "%f", "duration_per_op":"%s"}}`
	fmt.Printf(m+"\n", iterations, duration, float64(iterations)/duration.Seconds(), time.Duration(duration.Nanoseconds()/int64(iterations)))
}
