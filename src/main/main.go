package main

import (
	"log"

	"gitlab.com/flaneurtv/samm-benchmark/app"
	"gitlab.com/flaneurtv/samm-benchmark/config"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("Can't parse environment vars: %s", err)
		return
	}
	service := app.NewService(cfg)
	service.Run()
}
