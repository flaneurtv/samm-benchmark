package config

import (
	"os"

	"github.com/Netflix/go-env"
	samm "gitlab.com/flaneurtv/samm-client-go"
)

// Config holds application configuration data
type Config struct {
	ServiceName string `env:"SERVICE_NAME"`
	ServiceUUID string `env:"SERVICE_UUID"`
	ServiceHost string `env:"SERVICE_HOST"`

	NamespaceListener  string `env:"NAMESPACE_LISTENER"`
	NamespacePublisher string `env:"NAMESPACE_PUBLISHER"`
	ChannelUUID        string `env:"CHANNEL_UUID"`
	PlayoutUUID        string `env:"PLAYOUT_UUID"`

	SammConfig *samm.Config
}

// GetConfig gets config from environment
func GetConfig() (*Config, error) {
	c := &Config{}
	_, err := env.UnmarshalFromEnviron(c)
	c.SammConfig = samm.DefaultConfig
	c.ServiceHost, _ = os.Hostname()
	return c, err
}
