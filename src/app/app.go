package app

import (
	"fmt"

	"gitlab.com/flaneurtv/samm-benchmark/config"
	"gitlab.com/flaneurtv/samm-benchmark/handler"
	"gitlab.com/flaneurtv/samm-client-go"
)

// Service main application engine
type Service struct {
	config *config.Config
	samm   samm.Client
}

// NewService creates main application object
func NewService(config *config.Config) Service {
	return Service{config: config, samm: samm.NewClient(config.SammConfig)}
}

// Run runs application
func (s *Service) Run() {
	handler := &handler.CommandHandler{Config: s.config}

	router := samm.NewRouter()
	router.SetErrorHandler(s.errorHandler)
	router.RegisterTopic("trigger", handler.HandleBenchmarkTrigger)
	router.RegisterTopic("ping", handler.HandleBenchmarkPing)
	router.RegisterTopic("tick", handler.HandleBenchmarkTick)

	s.samm.Listen(router)
}

func (s *Service) errorHandler(command *samm.Message, writer samm.ResponseWriter) {
	writer.WriteError(fmt.Errorf("Can't process topic %s", command.Topic))
}
