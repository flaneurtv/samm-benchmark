package samm

import (
	"encoding/json"
	"log"
	"time"
)

// TimeFormat describes time format for samm
const TimeFormat = "2006-01-02T15:04:05.999Z"

// ErrorMessage describes error response for SAMM
type ErrorMessage struct {
	LogLevel   string `json:"log_level"`
	LogMessage string `json:"log_message"`
	CreatedAt  string `json:"created_at"`
}

type responseWriter struct {
	config *Config
}

func (r *responseWriter) WriteObject(object *Message) {
	b, err := json.Marshal(object)
	if err != nil {
		log.Printf("Can't marshal response object %s", err)
		return
	}

	str := string(b) + "\n"
	_, err = r.config.ObjectOutput.Write([]byte(str))
	if err != nil {
		log.Printf("Can't write %s", err)
		return
	}
}

func (r *responseWriter) WriteError(err error) {
	message := &ErrorMessage{
		LogLevel:   "error",
		LogMessage: err.Error(),
		CreatedAt:  time.Now().Format(TimeFormat),
	}

	b, err := json.Marshal(message)
	if err != nil {
		log.Printf("Can't marshal response object %s", err)
		return
	}
	str := string(b) + "\n"
	_, err = r.config.ErrorOutput.Write([]byte(str))
	if err != nil {
		log.Printf("Can't write %s", err)
		return
	}
}
