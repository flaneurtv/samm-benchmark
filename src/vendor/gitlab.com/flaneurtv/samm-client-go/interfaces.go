package samm

import (
	"encoding/json"
)

// Message describes message that describes command or successful response to SAMM
type Message struct {
	Topic       string          `json:"topic"`
	ServiceUUID string          `json:"service_uuid,omitempty"`
	ServiceName string          `json:"service_name,omitempty"`
	ServiceHost string          `json:"service_host,omitempty"`
	CreatedAt   string          `json:"created_at,omitempty"`
	Payload     json.RawMessage `json:"payload,omitempty"`
}

// ResponseWriter writes response for SAMM
type ResponseWriter interface {
	WriteObject(message *Message)
	WriteError(err error)
}

// CommandHandler handles command from SAMM
type CommandHandler interface {
	Handle(command *Message, writer ResponseWriter)
}

// Client listens for SAMM commands and sends them to processor
type Client interface {
	Listen(handler CommandHandler)
}

// TopicHandler handler for specific topic
type TopicHandler func(command *Message, writer ResponseWriter)

// Router sends command to handler by topic
type Router interface {
	CommandHandler
	RegisterTopic(topic string, handler TopicHandler)
	SetErrorHandler(handler TopicHandler)
}
