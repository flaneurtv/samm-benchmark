package samm

import (
	"strings"
)

// NewRouter returns new router object
func NewRouter() Router {
	return &topicRouter{routes: make(map[string]TopicHandler)}
}

type topicRouter struct {
	routes       map[string]TopicHandler
	errorHandler TopicHandler
}

// RegisterTopic registers topic in router
func (r *topicRouter) RegisterTopic(topic string, handler TopicHandler) {
	r.routes[topic] = handler
}

// SetErrorHandler sets handler for routing errors
func (r *topicRouter) SetErrorHandler(handler TopicHandler) {
	r.errorHandler = handler
}

// Handle processes each command
func (r *topicRouter) Handle(command *Message, writer ResponseWriter) {
	s := strings.Split(command.Topic, "/")
	if len(s) < 2 {
		r.errorHandler(command, writer)
		return
	}

	handler, ok := r.routes[s[1]]
	if ok {
		handler(command, writer)
	} else {
		r.errorHandler(command, writer)
	}
}
