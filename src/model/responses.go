package model

// TickPayload is a payload and response on tick message
type BenchmarkTrigger struct {
	Iterations int `json:"iterations"`
}

type BenchmarkPing struct {
	BenchmarkUUID string `json:"uuid"`
	PingIndex     int `json:"index"`
	PingTimestamp string `json:"timestamp"`
}

type Tick struct {
	TickUUID      string `json:"tick_uuid"`
	TickTimestamp string `json:"tick_timestamp"`
}
